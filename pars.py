# -*- coding: utf-8 -*-
__author__ = 'Freeman'

from lxml import html
import requests, sqlite3, os

PATH = os.path.dirname(os.path.abspath(__file__)) + '/'
DB_NAME = 'cars_base.db'

class DBModel():

    def __init__(self):
        self.conn = sqlite3.connect(PATH + DB_NAME)
        # возможно это новая база и надо создать таблицу.
        self.make_tables()

        # списки
        self.modelslist = []
        self.brandslist = []
        self.submodelslist = []
        self.modifylist = []
        # словари
        self.brands = {}
        self.models = {}
        self.submodels = {}
        self.urlsModelSubmodels = {}

    def query_with_commit(self, query):
        self.error = None
        self.make_query(query)
        try:
            self.conn.commit()
        except Exception as e:
            self.conn.rollback()
        finally:
            self.conn.close()

    def make_query(self, query):
        self.cursor = self.conn.execute(query)

    def make_tables(self):
        try:
            with open(PATH + 'sql.txt', 'r') as myfile:
                sqltext = myfile.read().replace('\n', '')
            self.conn.executescript(sqltext)
        except Exception as e:
            print('error is ' + str(e))

    def brads_dict(self):
        q = "SELECT ID, NAME, URL FROM auto_brands"
        self.make_query(q)
        for row in self.cursor:
            self.brands.update({
                "{}".format(row[2]): {
                    'id': int('{}'.format(row[0])),
                    'name': "{}".format(row[1])}
            })

            # список брендов
            self.brandslist.append(row[1])

    def model_dict(self):
        q = "SELECT auto_models.ID, auto_models.NAME, auto_models.BRAND_ID, auto_models.URL, auto_brands.NAME FROM auto_models LEFT JOIN auto_brands ON BRAND_ID = auto_brands.ID ORDER BY auto_brands.name"
        #q = "SELECT auto_models.ID, auto_models.NAME, auto_models.BRAND_ID, auto_models.URL, auto_brands.NAME FROM auto_models LEFT JOIN auto_brands ON BRAND_ID = auto_brands.ID WHERE auto_brands.NAME = 'Audi'"
        self.make_query(q)
        for row in self.cursor:
            # словарь моделей
            self.models.update({
                "{}".format(row[3]): {
                    'id': int('{}'.format(row[0])),
                    'name': "{}".format(row[1]),
                    'brand_id': "'{}'".format(row[2])
                }
            })
            # список моделей
            self.modelslist.append([row[1], row[4]])

    def submodels_dict(self):
        q = "SELECT auto_submodels.NAME, auto_submodels.ID, auto_submodels.BRAND_ID, MODEL_ID, auto_submodels.URL, auto_models.NAME as modelname FROM auto_submodels LEFT JOIN auto_models ON MODEL_ID = auto_models.ID"
        self.make_query(q)
        for row in self.cursor:
            # словарь submodels
            self.submodels.update({
                "{}".format(row[4]): {
                    'id': int('{}'.format(row[1])),
                    'name': "'{}'".format(row[0]),
                    'brand_id': int("{}".format(row[2])),
                    'model_id': int("{}".format(row[3])),
                    'modelname': "'{}'".format(row[5])
                }
            })
            # список подмоделей
            self.submodelslist.append([row[0], row[5]])

    def urls_for_modify(self):
        """
        запрос возвращает таблицу моделей и подмоделей по котом будут получены мобификации
        """
        q = "SELECT NAME, URL, BRAND_ID, ID as MODEL_ID, NULL as SUBMODEL_ID, 'model' as SOURCE FROM (SELECT NAME, BRAND_ID, ID, URL FROM auto_models WHERE ID NOT IN (SELECT MODEL_ID from auto_submodels )) UNION SELECT NAME, URL, BRAND_ID, MODEL_ID, ID, 'submodel' FROM auto_submodels"
        #q = "SELECT NAME, URL, BRAND_ID, ID as MODEL_ID, NULL as SUBMODEL_ID, 'model' as SOURCE FROM (SELECT NAME, BRAND_ID, ID, URL FROM auto_models WHERE ID NOT IN (SELECT MODEL_ID from auto_submodels )) UNION SELECT NAME, URL, BRAND_ID, MODEL_ID, ID, 'submodel' FROM auto_submodels LIMIT 5"
        #q = "SELECT * FROM (SELECT NAME, URL, BRAND_ID, ID as MODEL_ID, NULL as SUBMODEL_ID, 'model' as SOURCE FROM (SELECT NAME, BRAND_ID, ID, URL FROM auto_models WHERE ID NOT IN (SELECT MODEL_ID from auto_submodels )) UNION SELECT NAME, URL, BRAND_ID, MODEL_ID, ID, 'submodel' FROM auto_submodels) WHERE  SUBMODEL_ID in (471,472,473)"
        self.make_query(q)
        for row in self.cursor:
            # словарь urlsModelSubmodels
            self.urlsModelSubmodels.update({
                "{}".format(row[1]): {
                    'name': "'{}'".format(row[0]),
                    'brand_id': "{}".format(row[2]),
                    'model_id': "{}".format(row[3]),
                    'submodel_id': "{}".format(row[4]),
                    'source': "'{}'".format(row[5])
                }
            })
        pass

        # заполним список существующих модификаций
        q = "SELECT NAME, BRAND_ID, MODEL_ID, DRIVE_TYPE, ENGINE_TYPE FROM auto_modify"
        self.make_query(q)
        for row in self.cursor:
            self.modifylist.append([
                str(x) for x in row
            ])

class GetCars():

    def __init__(self):
        self.brandsdicts = []
        self.modelsdicts = []
        self.submodelsdicts = []
        self.modif = []
        self.desc = []

    def __get_tree__(self, link):
        page = requests.get('http://spravochniki.info{}'.format(link))
        tree = html.fromstring(page.content)
        return tree

    # удаление символа
    def clearName(self, obj):
        return obj.lstrip('\n').rstrip('\n').replace("'", "")

    def brandsIntoDb(self):
        if self.brandsdicts:
            begin_query = "INSERT INTO 'auto_brands' ('name', 'url') VALUES"
            query = ''
            i = 0
            for row in self.brandsdicts:

                # если это последня итерация
                if i != len(self.brandsdicts) - 1:
                    query = ''.join([query, "(",
                                     "'{}',".format(row['maker']),
                                     "'{}'".format(row['urlmaker']),
                                     "),", "\n"])
                else:
                    query = ''.join([query, "(",
                                     "'{}',".format(row['maker']),
                                     "'{}'".format(row['urlmaker']),
                                     ");"])

                i = i + 1

            query = begin_query + "\n" + query
            db = DBModel()
            db.query_with_commit(query)
        else:
            print('ZERO new brands')

    def modelsIntoDb(self):
        if self.modelsdicts:
            begin_query = "INSERT INTO 'auto_models' ('name', 'url', 'brand_id') VALUES"
            query = ''
            i = 0
            for row in self.modelsdicts:

                # если это последняя итерация
                if i != len(self.modelsdicts) - 1:
                    query = ''.join([query,
                                     "(",
                                     "'{}',".format(row['model']),
                                     "'{}',".format(row['urlmodel']),
                                     "{}".format(row['brand_id']),
                                     "),",
                                     "\n"])
                else:
                    query = ''.join([query,
                                     "(",
                                     "'{}',".format(row['model']),
                                     "'{}',".format(row['urlmodel']),
                                     "{}".format(row['brand_id']),
                                     ");"])

                i = i + 1

            query = begin_query + "\n" + query
            db = DBModel()
            db.query_with_commit(query)
        else:
            print("ZERO new models")

    def submodelsIntoDb(self):
        if self.submodelsdicts:
            begin_query = "INSERT INTO 'auto_submodels' ('name', 'url', 'brand_id', 'model_id') VALUES"
            query = ''
            i = 0
            for row in self.submodelsdicts:

                # если это последняя итерация
                if i != len(self.submodelsdicts) - 1:
                    query = ''.join([query,
                                     "(",
                                     "'{}',".format(row['submodel']),
                                     "'{}',".format(row['urlsubmodel']),
                                     "{},".format(row['brand_id']),
                                     "{}".format(row['model_id']),
                                     "),",
                                     "\n"])
                else:
                    query = ''.join([query,
                                     "(",
                                     "'{}',".format(row['submodel']),
                                     "'{}',".format(row['urlsubmodel']),
                                     "{},".format(row['brand_id']),
                                     "{}".format(row['model_id']),
                                     ");"])

                i = i + 1

            query = begin_query + "\n" + query
            db = DBModel()
            db.query_with_commit(query)
        else:
            print('ZERO new submodels')

    def modifyIntoDb(self):
        if self.modif:
            begin_query = "INSERT INTO 'auto_modify' ('name', 'url', 'brand_id', 'model_id', 'submodel_id', 'engine_type', 'drive_type', 'release_from', 'release_to') VALUES"
            query = ''
            i = 0
            for row in self.modif:

                # если это последняя итерация
                if i != len(self.modif) - 1:
                    query = ''.join([query,
                                     "(",
                                     "'{}',".format(row['modifname']),
                                     "'{}',".format(row['urlmodif']),
                                     "{},".format(row['brand_id']),
                                     "{},".format(row['model_id']),
                                     "{},".format(row['submodel_id']),
                                     "'{}',".format(row['enginetype']),
                                     "'{}',".format(row['wheeldrive']),
                                     "{},".format(row['from']),
                                     "{}".format(row['to']),
                                     "),",
                                     "\n"])
                else:
                    query = ''.join([query,
                                     "(",
                                     "'{}',".format(row['modifname']),
                                     "'{}',".format(row['urlmodif']),
                                     "{},".format(row['brand_id']),
                                     "{},".format(row['model_id']),
                                     "{},".format(row['submodel_id']),
                                     "'{}',".format(row['enginetype']),
                                     "'{}',".format(row['wheeldrive']),
                                     "{},".format(row['from']),
                                     "{}".format(row['to']),
                                     ");"])

                i = i + 1

            query = begin_query + "\n" + query
            db = DBModel()
            db.query_with_commit(query)

        else:
            print('ZERO new modify')

    def get_brands(self):
        db = DBModel()
        db.brads_dict()

        tree = self.__get_tree__('/auto')
        brands = tree.xpath('//a[@class="brand"]/text()')

        for name in brands:
            clrname = self.clearName(name)
            if clrname:
                if clrname not in db.brandslist:
                    link = tree.xpath('//a[text()="%s"]/@href' % name)
                    self.brandsdicts.append({'maker': "{}".format(self.clearName(name)), 'urlmaker': "{}".format(link[0])})

                    print('new Brand ' + clrname)

        self.brandsIntoDb()

    def get_models(self):
        db = DBModel()
        db.brads_dict()
        db.model_dict()

        for brandurl, param in db.brands.items():

            tree = self.__get_tree__(str(brandurl))

            # список названий моделей
            modelsnames = tree.xpath('//a[@class="model"]/text()')

            for model in modelsnames:
                clrname = self.clearName(model)
                brand_name = param['name']
                searchlist = [clrname, brand_name]

                if clrname :
                    if searchlist not in db.modelslist:
                        link = tree.xpath('//a[text()="%s"]/@href' % model)
                        self.modelsdicts.append(
                            {'model': "{}".format(self.clearName(model)),
                             'urlmodel': "{}".format(link[0]),
                             'brand_id': param['id']
                             }
                        )
                        print(' new Model ' +clrname)


        self.modelsIntoDb()

    def get_submodels(self):
        db = DBModel()
        db.model_dict()
        db.submodels_dict()
        for urlmodel, param in db.models.items():
            tree = self.__get_tree__(urlmodel)
            submodels_names = tree.xpath('//a[@class="submodel"]/text()')

            if len(submodels_names) > 0:
                for submodel in submodels_names:
                    clrname = self.clearName(submodel)
                    modelname = param['name']
                    searchlist = [clrname, modelname]

                    if clrname:
                        if searchlist not in db.submodelslist:
                            link = tree.xpath('//a[text()="%s"]/@href' % submodel)
                            self.submodelsdicts.append({
                                'submodel': "{}".format(clrname),
                                'urlsubmodel': "{}".format(link[0]),
                                'brand_id': param['brand_id'],
                                'model_id': param['id'],
                            })

                            print('     new subModel ' +clrname)

        self.submodelsIntoDb()

    def get_modif(self):
        db = DBModel()
        db.urls_for_modify()
        for url, params in db.urlsModelSubmodels.items():
            tree = self.__get_tree__(url)
            modiftables = tree.xpath('//table[@class="tbl"]//tr')[1:]

            for table in modiftables:
                subtable = table.xpath('td')
                modif_rslt = []
                for x in range(1, len(subtable)):
                    modif_rslt.append( ''.join(subtable[x].xpath('text()')) )

                modif_name = table.xpath('td')[0].xpath('a/text()')
                modif_url = table.xpath('td')[0].xpath('a/@href')

                # бывает нет даты окончания выпуска. добавим пустое значение
                for x in range(len(modif_rslt), 4, 1):
                    modif_rslt.append(0)

                #в тех случаях когда модификация для модели, значение подмодели заполним NULL
                try:
                    id_submodels = int(params['submodel_id'])
                except:
                    id_submodels = 'NULL'

                #проверим есть ли такие модификации уже есть в базе, то пропустим
                clrname = self.clearName(modif_name[0])
                brand_id = params['brand_id']
                model_id = params['model_id']
                drivetype = modif_rslt[1]
                enginetype = modif_rslt[0]
                searchlist = [clrname, brand_id, model_id, drivetype, enginetype]

                try:
                    realesfrom = int(modif_rslt[2])
                except:
                    realesfrom = 0

                try:
                    realesto = int(modif_rslt[3])
                except:
                    realesto = 0


                if searchlist not in db.modifylist:

                    self.modif.append({'modifname': "{}".format(modif_name[0]),
                                       'enginetype': "{}".format(modif_rslt[0]),
                                       'wheeldrive': "{}".format(modif_rslt[1]),
                                       'from': realesfrom,
                                       'to': realesto,
                                       'urlmodif': "{}".format(modif_url[0]),
                                        'brand_id': int(params['brand_id']),
                                        'model_id': int(params['model_id']),
                                        'submodel_id': id_submodels
                                       })

                    print('         new Modify ' + clrname)

        self.modifyIntoDb()

if __name__ == '__main__':
    cars = GetCars()
    cars.get_brands()
    cars.get_models()
    cars.get_submodels()
    cars.get_modif()


